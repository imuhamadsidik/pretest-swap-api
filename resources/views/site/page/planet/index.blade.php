@extends('layout.layout')
@section('content') 
		
	<!-- CONTENT -->
    <!--=== Slider ===-->
    <div id="content">  
	    <br><br>
    	<div class="cbp-panels"> 
        <div class="cbp-l-slider-title-block">
            <div>THE PLANETS ({{ $count }})</div>
        </div> 
        <div id="planets" class="cbp">
        	@foreach($results as $row)   
	            <div class="cbp-item ">
	                <a href="{{ str_replace(API_URL.'planets',URL::to('swapi/planets'),$row->url) }}"  class="cbp-caption">
	                    <div class="cbp-caption-defaultWrap">
	                        <img src="{{ asset('assets/images/item-4.jpg') }}" alt="">
	                    </div>
	                    <div class="cbp-caption-activeWrap">
	                        <div class="cbp-l-caption-alignCenter">
	                            <div class="cbp-l-caption-body">
	                                <div class="cbp-l-caption-text">VIEW DETAIL</div>
	                            </div>
	                        </div>
	                    </div>
	                </a>
	                <a href="{{ str_replace(API_URL.'planets',URL::to('swapi/planets'),$row->url) }}"  class="cbp-l-grid-blog-title">{{ $row->name }}</a>
	                <div class="cbp-l-grid-blog-date">Diameter : {{ number_format($row->diameter,2) }}</div>
	                <div class="cbp-l-grid-blog-split">|</div>
	                <a  class="cbp-l-grid-blog-comments">Population : {{  number_format((int)$row->population,2) }}</a> 
	            </div> 
            @endforeach
        </div> 

        <nav>
		  <ul class="pager">
		    <li><a href="{{ $previous }}">Previous</a></li>
		    <li><a href="{!! $next !!}">Next</a></li>
		  </ul>
		</nav>
  
@stop
@extends('layout.layout')
@section('content') 
		
	<!-- CONTENT -->
    <!--=== Slider ===-->
    <div id="content"> 
	    <div class="tp-banner-container">
	        <div class="tp-banner">
	            <ul>
	                <!-- SLIDE -->
	                <li class="revolution-mch-1" data-transition="fade" data-slotamount="5" data-masterspeed="1000" data-title="Slide 1">
	                    <!-- MAIN IMAGE -->
	                    <img src="{{ asset('assets/images/1.jpg') }}"  alt="darkblurbg"   data-bgposition="center" data-bgrepeat="no-repeat">

	                    <div class="tp-caption revolution-ch1 sft start"
	                        data-x="center"
	                        data-hoffset="0"
	                        data-y="100"
	                        data-speed="1500"
	                        data-start="500"
	                        data-easing="Back.easeInOut"
	                        data-endeasing="Power1.easeIn"                        
	                        data-endspeed="300">
	                        Stars<br>
	                        <strong>Wars</strong><br>
	                        is here
	                    </div>

	                    <!-- LAYER -->
	                    <div class="tp-caption sft"
	                        data-x="center"
	                        data-hoffset="0"
	                        data-y="380"
	                        data-speed="1600"
	                        data-start="1800"
	                        data-easing="Power4.easeOut"
	                        data-endspeed="300"
	                        data-endeasing="Power1.easeIn"
	                        data-captionhidden="off"
	                        style="z-index: 6">
	                        <a href="#" class="btn-u btn-brd btn-brd-hover btn-u-light">Watch Now</a>
	                    </div>
	                </li>
	                <!-- END SLIDE -->

	                <!-- SLIDE -->
	                <li class="revolution-mch-1" data-transition="fade" data-slotamount="5" data-masterspeed="1000" data-title="Slide 2">
	                    <!-- MAIN IMAGE -->
	                    <img src="{{ asset('assets/images/2.jpg') }}"  alt="darkblurbg"   data-bgposition="center" data-bgrepeat="no-repeat">

	                    <div class="tp-caption revolution-ch3 sft start"
	                        data-x="center"
	                        data-hoffset="0"
	                        data-y="140"
	                        data-speed="1500"
	                        data-start="500"
	                        data-easing="Back.easeInOut"
	                        data-endeasing="Power1.easeIn"                        
	                        data-endspeed="300">
	                        Latest <strong>Turtle</strong> Fighting
	                    </div>

	                    <!-- LAYER -->
	                    <div class="tp-caption revolution-ch4 sft"
	                        data-x="center"
	                        data-hoffset="-14"
	                        data-y="210"
	                        data-speed="1400"
	                        data-start="2000"
	                        data-easing="Power4.easeOut"
	                        data-endspeed="300"
	                        data-endeasing="Power1.easeIn"
	                        data-captionhidden="off"
	                        style="z-index: 6">
	                        Cras non dui et quam auctor pretium.<br>
	                        Aenean enim tortr, tempus et iteus m
	                    </div>

	                    <!-- LAYER -->
	                    <div class="tp-caption sft"
	                        data-x="center"
	                        data-hoffset="0"
	                        data-y="300"
	                        data-speed="1600"
	                        data-start="1800"
	                        data-easing="Power4.easeOut"
	                        data-endspeed="300"
	                        data-endeasing="Power1.easeIn"
	                        data-captionhidden="off"
	                        style="z-index: 6">
	                        <a href="#" class="btn-u btn-brd btn-brd-hover btn-u-light">Watch Now</a>
	                    </div>
	                </li>
	                <!-- END SLIDE -->

	                <!-- SLIDE -->
	                <li class="revolution-mch-1" data-transition="fade" data-slotamount="5" data-masterspeed="1000" data-title="Slide 3">
	                    <!-- MAIN IMAGE -->
	                    <img src="{{ asset('assets/images/3.jpg') }}"  alt="darkblurbg"    data-bgposition="center " data-bgrepeat="no-repeat">

	                    <div class="tp-caption revolution-ch3 sft start"
	                        data-x="right"
	                        data-hoffset="5"
	                        data-y="130"
	                        data-speed="1500"
	                        data-start="500"
	                        data-easing="Back.easeInOut"
	                        data-endeasing="Power1.easeIn"                        
	                        data-endspeed="300">
	                        <strong>Great</strong> Adventure
	                    </div>

	                    <!-- LAYER -->
	                    <div class="tp-caption revolution-ch4 sft"
	                        data-x="right"
	                        data-hoffset="0"
	                        data-y="210"
	                        data-speed="1400"
	                        data-start="2000"
	                        data-easing="Power4.easeOut"
	                        data-endspeed="300"
	                        data-endeasing="Power1.easeIn"
	                        data-captionhidden="off"
	                        style="z-index: 6">
	                        lectus. Cras non dui et quam auctor.<br>
	                        Aenean enim tortor, tempus et im
	                    </div>

	                    <!-- LAYER -->
	                    <div class="tp-caption sft"
	                        data-x="right"
	                        data-hoffset="0"
	                        data-y="300"
	                        data-speed="1600"
	                        data-start="2800"
	                        data-easing="Power4.easeOut"
	                        data-endspeed="300"
	                        data-endeasing="Power1.easeIn"
	                        data-captionhidden="off"
	                        style="z-index: 6">
	                        <a href="#" class="btn-u btn-brd btn-brd-hover btn-u-light">Watch Now</a>
	                    </div>
	                </li> 
	                <!-- END SLIDE -->   
	            </ul>
	            <div class="tp-bannertimer tp-bottom"></div>            
	        </div>
	    </div>


	    <br><br>
    	<div class="cbp-panels"> 
        <div class="cbp-l-slider-title-block">
            <div>BEST FILMS</div>
        </div> 
        <div id="films" class="cbp">
        	@foreach($Mresults as $row)   
	            <div class="cbp-item print motion">
	                <a href="{{ str_replace(API_URL.'films',URL::to('swapi/films'),$row->url) }}"  class="cbp-caption">
	                    <div class="cbp-caption-defaultWrap">
	                        <img src="{{ asset('assets/images/item-4.jpg') }}" alt="">
	                    </div>
	                    <div class="cbp-caption-activeWrap">
	                        <div class="cbp-l-caption-alignCenter">
	                            <div class="cbp-l-caption-body">
	                                <div class="cbp-l-caption-text">VIEW DETAIL</div>
	                            </div>
	                        </div>
	                    </div>
	                </a>
	                <a href="{{ str_replace(API_URL.'films',URL::to('swapi/films'),$row->url) }}"  class="cbp-l-grid-blog-title">{{ $row->title }}</a>
	                <div class="cbp-l-grid-blog-date">{{date('d M Y',strtotime($row->release_date))}}</div>
	                <div class="cbp-l-grid-blog-split">|</div>
	                <a  class="cbp-l-grid-blog-comments">Director {{ $row->director }}</a>
	                <div class="cbp-l-grid-blog-desc"> {{ str_limit($row->opening_crawl,200) }}</div>
	            </div> 
            @endforeach
        </div> 
  
@stop
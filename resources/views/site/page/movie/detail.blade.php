@extends('layout.layout')
@section('content') 
		
	<!-- CONTENT --> 
    <div id="content"> 
	    <div class="row">
            <div class="post-content post-classic post-content-single col-md-9">

                <div class="post-item">
                    <div class="post-image">
                        <a href="#">
                            <img alt="" src="{{ asset('assets/images/1.jpg') }}">
                        </a>
                    </div>
                    <div class="post-content-details">
                        <div class="post-title">
                            <h2>{{ $dataPage->title }}</h2>
                        </div>
                        <div class="post-info">
                            <span class="post-autor">Producer  :  {{ $dataPage->producer }} </span>
                            <span class="post-category"><br>Director  {{ $dataPage->director }} </span>
                            <span class="post-category"><br>Realease in {{date('d M Y',strtotime($dataPage->release_date))}} </span>
                        </div>
                        <div class="post-description">
                            {!! $dataPage->opening_crawl !!}
                        </div>
                    </div> 
                </div> 

				<div class="container">
					<div class="heading-fancy heading-line text-center">
						<h4>The Stars</h4>
					</div>
					<div class="row">
						{!! $stars !!}
					</div> 
				</div> 
 				<br>
                
                <div class="comment-form">
                    <div class="heading">
                        <h4>Leave a comment</h4>
                    </div>
                    <form class="form-gray-fields">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name" class="upper">Your Name</label>
                                    <input type="text" aria-required="true" id="name" placeholder="Enter name" name="senderName" class="form-control required">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="email" class="upper">Your Email</label>
                                    <input type="email" aria-required="true" id="email" placeholder="Enter email" name="senderEmail" class="form-control required email">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="phone" class="upper">Your Phone</label>
                                    <input type="text" aria-required="true" id="phone" placeholder="Enter phone" name="phone" class="form-control required">
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="comment" class="upper">Your comment</label>
                                    <textarea aria-required="true" id="comment" placeholder="Enter comment" rows="9" name="comment" class="form-control required"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group text-center">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-paper-plane"></i>&nbsp;Post comment</button>
                                </div>
                            </div>
                        </div> 
                    </form>
                </div>
            </div>
            <div class="sidebar sidebar-modern col-md-3">
                <!--widget newsletter-->
                <div class="widget clearfix widget-newsletter">
                    <form id="widget-subscribe-form-sidebar" action="include/subscribe-form.php" role="form" method="post" class="form-inline">
                        <h4 class="widget-title">Newsletter</h4>
                        <small>Stay informed on our news!</small>
                        <div class="input-group">
                            <input type="email" aria-required="true" name="widget-subscribe-form-email" class="form-control required email" placeholder="Enter your Email">
                            <span class="input-group-btn">
			                  <button type="submit" id="widget-subscribe-submit-button" class="btn btn-primary"><i class="fa fa-paper-plane"></i></button>
			                  </span> </div>
                    </form> 
                </div>
                <!--end: widget newsletter-->

                <!--widget tags -->
                <div class="widget clearfix widget-tags">
                    <h4 class="widget-title">Tags</h4>
                    <div class="tags">
                        <a href="#">Movie</a>
                        <a href="#">People</a>
                        <a href="#">Vehicle</a>
                        <a href="#">Planet</a>  
                    </div>
                </div>
                <!--end: widget tags -->
                
            </div>
        </div>
    </div>
   	</div>
  
@stop
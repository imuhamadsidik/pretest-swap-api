<!DOCTYPE html>
<html lang="en">

    <head> 
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}">
        {!!SEO::generate()!!}  
        <title>   </title> 
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}"> 
        @if(Request::is('films') or Request::is('/'))
        <link rel="stylesheet" href="{{ asset('assets/js/plugin/revolution-slider/rs-plugin/css/settings.css') }}">
        @endif
        <link rel="stylesheet" href="{{ asset('assets/js/plugin/cubeportfolio/css/cubeportfolio.min.css') }}" type="text/css">

        <!-- Custom styles for this template -->
        <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}"> 
    </head> 
    <body> 
    <header class="header">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="{{ URL::to('/') }}"><img src="{{ asset('assets/images/logo.png') }}"></a>
            </div> 
            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav navbar-right"> 
                <li {{ (Request::is('films*') ? 'class=active' : '') }}><a href="{{ URL::to('films') }}">Films</a></li>
                <li {{ (Request::is('planets*') ? 'class=active' : '') }}><a href="{{ URL::to('planets') }}">Planets</a></li>  
              </ul> 
            </div> 
        </nav> 
    </header>
    <!-- /HEDER -->
    <aside id="bar-left" class="bar-side"><div>© SWAP API </div></aside>
    <aside id="bar-right" class="bar-side right-side"><div class="right-side"> The Great Free API </div></aside>
     
    @yield('content')
    
    <footer>
        <p class="text-center">Copyright By Muhamad Sidik</p>
    </footer>       
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{ asset('assets/js/jquery.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>  
    <script src="{{ asset('assets/js/scrolltopcontrol.js') }}"></script>  

    <script src="{{ asset('assets/js/plugin/cubeportfolio/js/jquery.cubeportfolio.min.js') }}" type="text/javascript"></script>   
    @if(Request::is('films') or Request::is('/'))
    <script src="{{ asset('assets/js/plugin/revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugin/revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>
    <script src="{{ asset('assets/js/revolution-slider.js') }}"></script> 
    <script>
    jQuery(document).ready(function() { 
        RevolutionSlider.initRSfullWidth();      
    });
    </script>
    @endif
 
    <script type="text/javascript"> 
    (function($, window, document, undefined) {
        'use strict'; 
        // init cubeportfolio
        $('#films').cubeportfolio({
            layoutMode: 'slider',
            drag: true,
            auto: false,
            autoTimeout: 5000,
            autoPauseOnHover: true,
            showNavigation: true,
            showPagination: false,
            rewindNav: false,
            scrollByPage: false,
            gridAdjustment: 'responsive',
            mediaQueries: [{
                width: 1100,
                cols: 4
            }, {
                width: 800,
                cols: 3
            }, {
                width: 500,
                cols: 2
            }, {
                width: 320,
                cols: 1
            }],
            gapHorizontal: 0,
            gapVertical: 25,
            caption: 'overlayBottomReveal',
            displayType: 'lazyLoading',
            displayTypeSpeed: 100,

            // lightbox
            lightboxDelegate: '.cbp-lightbox',
            lightboxGallery: true,
            lightboxTitleSrc: 'data-title',
            lightboxCounter: '<div class="cbp-popup-lightbox-counter"> of</div>',

            // singlePage popup
            singlePageDelegate: '.cbp-singlePage',
            singlePageDeeplinking: true,
            singlePageStickyNavigation: true,
            singlePageCounter: '<div class="cbp-popup-singlePage-counter"> of </div>',
            singlePageAnimation: 'fade',
            singlePageCallback: function(url, element) {
                // to update singlePage content use the following method: this.updateSinglePage(yourContent)
                var indexElement = $(element).parents('.cbp-item').index(),
                    item = singlePage.eq(indexElement);

                this.updateSinglePage(item.html());
            },
        });

          // init cubeportfolio
            $('#planets').cubeportfolio({
                filters: '#filters-container',
                loadMore: '#loadMore-container',
                loadMoreAction: 'click',
                layoutMode: 'grid',
                defaultFilter: '*',
                animationType: 'quicksand',
                gapHorizontal: 35,
                gapVertical: 30,
                gridAdjustment: 'responsive',
                mediaQueries: [{
                    width: 1100,
                    cols: 4
                }, {
                    width: 800,
                    cols: 3
                }, {
                    width: 500,
                    cols: 2
                }, {
                    width: 320,
                    cols: 1
                }],
                caption: 'overlayBottomReveal',
                displayType: 'sequentially',
                displayTypeSpeed: 80,

                // lightbox
                lightboxDelegate: '.cbp-lightbox',
                lightboxGallery: true,
                lightboxTitleSrc: 'data-title', 

                // singlePage popup
                singlePageDelegate: '.cbp-singlePage',
                singlePageDeeplinking: true,
                singlePageStickyNavigation: true, 
                singlePageCallback: function(url, element) { 
                    var t = this;

                    $.ajax({
                            url: url,
                            type: 'GET',
                            dataType: 'html',
                            timeout: 5000
                        })
                        .done(function(result) {
                            t.updateSinglePage(result);
                        })
                        .fail(function() {
                            t.updateSinglePage("Error! Please refresh the page!");
                        });
                },
            }); 
        })(jQuery, window, document);
 
    </script>
  </body>
</html>

   


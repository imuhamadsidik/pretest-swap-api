<?php
namespace App\Http\Middleware;

use Closure; 
use Illuminate\Http\Response;

class VariableMiddleware {
	/**
  * Handle an incoming request.
  *
  * @param \Illuminate\Http\Request $request
  * @param \Closure $next
  * @return mixed
  */
	public function handle($request, Closure $next){  
		define('INITIAL_SITE','swapi'); 
		define('API_URL','http://swapi.co/api/'); //swapi

		return $next($request);
	}
}
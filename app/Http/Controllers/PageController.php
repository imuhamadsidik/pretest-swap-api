<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use SEO;
use SEOMeta;
use OpenGraph;
use Request;
use CodeZero\Courier\Courier;
use CodeZero\Courier\Exceptions\HttpExcepetion; 
use Session;
use App\Library\Library;

class PageController extends Controller{
	public $_now;
	public $_courier;
	public $_agent;
	public function __construct(Courier $Courier){
		$this->_courier = $Courier;
		$this->_now = date('Y-m-d',strtotime('NOW')); 
	}

  public function index(){
      SEO::SetTitle(' SWAP API');
      SEOMeta::SetKeywords(['movie api','planet api']);
      SEOMeta::SetDescription(' '); 

      $films = API_URL.'films/';
      $dataFilm = $this->_courier->get($films); 

      if($dataFilm->getBody()!="[]"){ 
       $dataFilmObj = $dataFilm->toObjects();
 
       $dataPage['Mnext']      = Library::nextPage($dataFilmObj->next);
       $dataPage['Mprevious']  = Library::nextPage($dataFilmObj->previous);
       $dataPage['Mresults'] = $dataFilmObj->results;
       $dataPage['Mcount'] = $dataFilmObj->count;
      }  

      return view('site.page.movie.index')->with($dataPage);
  }

  public function film_detail($id){
      $films = API_URL.'films/'.$id.'/';
      $dataFilm = $this->_courier->get($films); 
  
      if($dataFilm->getBody()!="[]"){ 
       $dataFilmObj = $dataFilm->toObjects(); 
      }  


      SEO::SetTitle(' SWAP API');
      SEOMeta::SetKeywords(['movie api','planet api']);
      SEOMeta::SetDescription(' '); 

      $data['dataPage'] = $dataFilmObj;
 
      $stars = "";
      $i=0;
      foreach($dataFilmObj->characters as $row) {
        $dataStars = $this->_courier->get($row);  
        if($dataStars->getBody()!="[]"){ 
         $dataStarsObj = $dataStars->toObjects(); 
        }
        if($i > 5) break;
        $stars .=  
        "<div class=\"col-md-4\">      
            <div class=\"widget-shop\">
              <div class=\"stars\">
                <div class=\"stars-image\">
                  <a href=\"#\"><img src=\"".url('assets/images/stars/a6.jpg')."\" alt=\"\">
                  </a>
                </div>
                <div class=\"stars-description\"> 
                  <div class=\"stars-title\">
                    <h3> ".$dataStarsObj->name." </h3>
                    <span>".$dataStarsObj->gender."</span><br>
                    <span>".$dataStarsObj->birth_year."</span>
                  </div> 
                </div>
              </div> 
            </div> 
          </div>";
          $i++;
      } 
                       
      $data['stars'] = $stars; 
      return view('site.page.movie.detail')->with($data); 
  }

  public function planets(){
      SEO::SetTitle(' SWAP API');
      SEOMeta::SetKeywords(['movie api','planet api']);
      SEOMeta::SetDescription(' ');
       
      $url = API_URL.'planets/';

      //pagination
      $page = Request::Input('page');
      if($page)
      $url = $url.'?page='.$page;

      $dataPlanet = $this->_courier->get($url); 

      if($dataPlanet->getBody()!="[]"){ 
       $dataObj = $dataPlanet->toObjects(); 
       $dataPage['next']      = Library::nextPage($dataObj->next);
       $dataPage['previous']  = Library::nextPage($dataObj->previous);
       $dataPage['results'] = $dataObj->results;
       $dataPage['count'] = $dataObj->count;
      }  

      return view('site.page.planet.index')->with($dataPage);
  }

   public function planet_detail($id){
      $url = API_URL.'planets/'.$id.'/';
      $dataUrl = $this->_courier->get($url); 
  
      if($dataUrl->getBody()!="[]"){ 
       $dataUrlObj = $dataUrl->toObjects(); 
      }  


      SEO::SetTitle(' SWAP API');
      SEOMeta::SetKeywords(['movie api','planet api']);
      SEOMeta::SetDescription(' '); 

      $data['dataPage'] = $dataUrlObj;

      $films = "";
      $i=0;
      foreach($dataUrlObj->films as $row) {
        $dataFilms = $this->_courier->get($row);  

        if($dataFilms->getBody()!="[]"){ 
         $dataFilmsObj = $dataFilms->toObjects(); 
        }
        // dd($dataFilmsObj->title);
        if($i > 5) break;
        $films .=  
        "<div class=\"col-md-4\">      
            <div class=\"widget-shop\">
              <div class=\"stars\">
                <div class=\"stars-image\">
                  <a href=\"".str_replace(API_URL.'films',url('swapi/films'),$dataFilmsObj->url)."\"><img src=\"".url('assets/images/stars/a6.jpg')."\" alt=\"\">
                  </a>
                </div>
                <div class=\"stars-description\"> 
                  <div class=\"stars-title\">
                    <h3> ".$dataFilmsObj->title." </h3> 
                  </div> 
                </div>
              </div> 
            </div> 
          </div>";
          $i++;
      } 
                       
      $data['films'] = $films; 
      return view('site.page.planet.detail')->with($data); 
  }

  
}
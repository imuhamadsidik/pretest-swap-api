<?php
namespace App\Library;

use Request;

class Library{
	public static function nextPage($urlString){
		$currentUrl = Request::url();
		$explode = explode('?',$urlString);
		if(count($explode)>1)
			return $currentUrl.'?'.$explode[1];

		return '';			
	} 
	public static function cleanText($str){
		$str = str_replace('-', '', $str);
		$str = str_replace('  ', ' ', $str);
		$str = str_replace(' ', '-', $str);
		return strtolower($str);
	}
}